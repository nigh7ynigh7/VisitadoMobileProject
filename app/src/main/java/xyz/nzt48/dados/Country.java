package xyz.nzt48.dados;

import android.app.Activity;
import android.os.AsyncTask;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by d4rk on 10/1/2016.
 */

public class Country {

    //criar lista para todos os paises beixados
    public static ArrayList<Country> PAISES = null;

    private int id;
    private String iso;
    private String shortname;
    private String longname;
    private String callingCode;
    private int status ;
    private String culture;





    public static void CriarPaises(String json)
    {
        try
        {
            //converter o JSON para uma lista de paises.
            Country.PAISES =
                    new Gson().fromJson(
                            json,
                            new TypeToken<ArrayList<Country>>(){}.getType());
        }catch(Exception e)
        {
            //Caso o processo no try{} de erro, retorna null.
            Country.PAISES = null;
        }
    }



    // Getters e setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getLongname() {
        return longname;
    }

    public void setLongname(String longname) {
        this.longname = longname;
    }

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCulture() {
        return culture;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }


    @Override
    public String toString() {
        return this.shortname;
    }
}
