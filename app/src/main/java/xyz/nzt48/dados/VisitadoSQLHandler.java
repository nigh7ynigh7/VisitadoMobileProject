package xyz.nzt48.dados;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;



public class VisitadoSQLHandler extends SQLiteOpenHelper {


    private static final String DB_NAME = "visitado";
    private static final int DB_VERSION = 1;

    public VisitadoSQLHandler(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE Visitado (" +
                    "visitadoId INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "countryId INTEGER," +
                    "iso TEXT," +
                    "shortname TEXT," +
                    "longname TEXT," +
                    "callingCode TEXT," +
                    "status  INTEGER," +
                    "culture TEXT," +
                    "dataVisitado INTEGER)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    private static ContentValues getVistadoValues(Visitado visitado)
    {
        ContentValues vistadoValues = new ContentValues();

        //insert values here for shuipping!
        vistadoValues.put("countryId",
                visitado.getCountryId() != 0 ? visitado.getCountryId() : 0);
        vistadoValues.put("iso",
                visitado.getIso() != null ? visitado.getIso() : "");
        vistadoValues.put("shortname",
                visitado.getShortname() != null ? visitado.getShortname() : "");
        vistadoValues.put("longname",
                visitado.getLongname() != null ? visitado.getLongname() : "");
        vistadoValues.put("callingCode",
                visitado.getCallingCode() != null ? visitado.getCallingCode() : "");
        vistadoValues.put("status",
                visitado.getStatus() != 0 ? visitado.getStatus() : 0);
        vistadoValues.put("culture",
                visitado.getCulture() != null ? visitado.getCulture() : "");
        vistadoValues.put("dataVisitado",
                visitado.getDataVisitadoAsInt() != 0 ? visitado.getDataVisitadoAsInt() : 0);

        return vistadoValues;
    }


    public static void insertVisitado(SQLiteDatabase db, Visitado visitado)
    {
        db.insert("Visitado", null, getVistadoValues(visitado));
    }

    public static void atualizarVisitado(SQLiteDatabase db, Visitado visitado)
    {
        db.update("Visitado", getVistadoValues(visitado), "visitadoId = ?" , new String[]{Integer.toString(visitado.getVisitadoId())});
    }

    public static void wipeVisitado(SQLiteDatabase db)
    {
        db.delete("Visitado",null, null);
    }

    private static Visitado fromCursor(Cursor c)
    {
        Country country = new Country();

        country.setId(c.getInt(c.getColumnIndex("countryId")));
        country.setStatus(c.getInt(c.getColumnIndex("status")));
        country.setCallingCode(c.getString(c.getColumnIndex("callingCode")));
        country.setLongname(c.getString(c.getColumnIndex("longname")));
        country.setShortname(c.getString(c.getColumnIndex("shortname")));
        country.setIso(c.getString(c.getColumnIndex("iso")));
        country.setCulture(c.getString(c.getColumnIndex("culture")));

        Visitado v = new Visitado(country);
        v.setVisitadoId(c.getInt(c.getColumnIndex("visitadoId")));
        v.setDataVisitadoAsInt(c.getLong(c.getColumnIndex("dataVisitado")));

        return v;
    }

    public static Visitado getVisitadoById(SQLiteDatabase db, int id)
    {

        Cursor c = db.rawQuery("SELECT * FROM Visitado WHERE visitadoId = "+id, null);

        if(c.getCount() == 0)
            return null;

        c.moveToFirst();

        return fromCursor(c);


    }

    public static ArrayList<Visitado> listaPaisesVisitados(SQLiteDatabase db)
    {
        Cursor c = db.rawQuery("SELECT * FROM Visitado", null);

        if(c.getCount() == 0)
            return null;

        c.moveToFirst();

        ArrayList<Visitado> visitados = new ArrayList<>();

        do{
            visitados.add(fromCursor(c));
        }while(c.moveToNext());

        return visitados;
    }

    public static void deletarVisitado(SQLiteDatabase db, int id)
    {
        db.delete("Visitado", "visitadoId = ?", new String[]{id + ""});
    }

    public static void deletarVisitado(SQLiteDatabase db, Visitado visitado)
    {
        deletarVisitado(db, visitado.getVisitadoId());
    }
}
