package xyz.nzt48.dados;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by d4rk on 10/6/2016.
 */

public class Visitado {
    private int visitadoId;
    private int countryId;
    private String iso;
    private String shortname;
    private String longname;
    private String callingCode;
    private int status;
    private String culture;
    private long dataVisitado;


    public Visitado(Country c) {
        this.setCountry(c);
    }

    public Visitado(Country c, Date data)
    {
        this.setCountry(c);
        this.setDataVisitado(data);
    }

    public void setCountry(Country c)
    {
        countryId = c.getId();
        iso = c.getIso();
        shortname = c.getShortname();
        longname = c.getLongname();
        callingCode = c.getCallingCode();
        status  = c.getStatus();
        culture = c.getCulture();
    }

    public int getVisitadoId() {
        return visitadoId;
    }

    public void setVisitadoId(int visitadoId) {
        this.visitadoId = visitadoId;
    }


    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getLongname() {
        return longname;
    }

    public void setLongname(String longname) {
        this.longname = longname;
    }

    public String getCallingCode() {
        return callingCode;
    }

    public void setCallingCode(String callingCode) {
        this.callingCode = callingCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCulture() {
        return culture;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public Date getDataVisitado() {
        return new Date(this.dataVisitado);
    }

    public long getDataVisitadoAsInt()
    {
        return dataVisitado;
    }

    public void setDataVisitado(Date dataVisitado) {
        this.dataVisitado = dataVisitado.getTime();
    }

    public void setDataVisitadoAsInt(long dataVisitado) {
        this.dataVisitado = dataVisitado;
    }


    @Override
    public String toString() {

        Calendar c = Calendar.getInstance();

        c.setTime(new Date(dataVisitado));

        StringBuilder b = new StringBuilder();
        b.append(shortname);
        b.append('(');
        b.append(c.get(Calendar.YEAR));
        b.append("/");
        b.append(c.get(Calendar.MONTH)+1);
        b.append("/");
        b.append(c.get(Calendar.DAY_OF_MONTH));
        b.append(')');

        return b.toString();
    }
}
