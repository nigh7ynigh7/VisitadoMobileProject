package xyz.nzt48.visitado;


import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import xyz.nzt48.dados.Visitado;
import xyz.nzt48.dados.VisitadoSQLHandler;

public class VisitadosFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public VisitadosFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static VisitadosFragment newInstance(String param1, String param2) {
        VisitadosFragment fragment = new VisitadosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        //OBTER LISTA DE PAISES VISITADO
        new GetCountryList().execute();
        return inflater.inflate(R.layout.fragment_visitados, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    //OBTER LISTA DO BANCO!
    class GetCountryList extends AsyncTask<Void, Void, ArrayList<Visitado>> {


        @Override
        protected ArrayList<Visitado> doInBackground(Void... params) {

            //OBTER LISTA DO BANCO E COLOCAR NO ARRAYLIST
            VisitadoSQLHandler cs = new VisitadoSQLHandler(getContext());
            SQLiteDatabase db = cs.getReadableDatabase();
            ArrayList<Visitado> v = VisitadoSQLHandler.listaPaisesVisitados(db);
            db.close();

            if(v == null)
                v = new ArrayList<>();

            return v;
        }

        @Override
        protected void onPostExecute(final ArrayList<Visitado> result){

            ListView lv = (ListView) getView().findViewById(R.id.listaVisitados);
            if(lv == null) return;

//            ArrayAdapter<Visitado> adapter = new ArrayAdapter<>(
//                    lv.getContext(),
//                    android.R.layout.simple_list_item_1,
//                    result);

            //CRIAR ADAPTADOR PARA COLOCAR NO LISTVIEW
            CountryVisitadoAdapter adapter = new CountryVisitadoAdapter(getContext(),
                    R.layout.list_view_row, result);


            lv.setAdapter(adapter);

            //CRIAR EVENTOS DE TOQUE PARA O LISTVIEW
            AdapterView.OnItemClickListener ouvidor = new AdapterView.OnItemClickListener(){
                public void onItemClick(AdapterView<?> listView,
                                        View itemView,
                                        int position,
                                        long id) {

                    //ABRIR VISITADOACTIVITY PARA O PAIS VISITADO
                    Intent intent = new Intent(getActivity(), VisitadoActivity.class);
                    intent.putExtra("tipo", "edit");
                    intent.putExtra("id",  result.get((int) id).getVisitadoId());
                    startActivity(intent);

                }
            };

            lv.setOnItemClickListener(ouvidor);
        }
    }
}
