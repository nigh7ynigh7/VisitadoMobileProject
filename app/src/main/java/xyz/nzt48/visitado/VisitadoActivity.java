package xyz.nzt48.visitado;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Date;

import xyz.nzt48.dados.Country;
import xyz.nzt48.dados.Visitado;
import xyz.nzt48.dados.VisitadoSQLHandler;


/*
*
* A ATIVIDADE QUE CONTEM AS INFORMACOES DO PAIS.
* OU TAMBEM, CONTEM AS INFORMACOES DO PAIS JA VISITADO.
*
*
* */

public class VisitadoActivity extends AppCompatActivity {

    //PAISES
    private Visitado visitado;
    private Country pais;

    //FATOR MULTIPLICATIVO DA BANDEIRA
    private double factor = 4.3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitado);


        //RECEBER AS INFORMACOES PRELIMINARES
        Intent i = getIntent();
        filtrarIntent(i);
    }

    protected void filtrarIntent(Intent i)
    {
        if(i == null)
            return;

        //VER O TIPO DE intent E O ID DO MESMO
        String tipo = i.getStringExtra("tipo");
        int id = i.getIntExtra("id", -1);

        //OBTER PAIS/VISITADO
        setVisitadoOuPais(tipo, id);
    }

    protected void setVisitadoOuPais(String tipo, int id)
    {
        //BUSCA TODOS OS OBJETOS DO VIEW
        TextView shortname = (TextView) findViewById(R.id.txtCountryName);
        TextView longname = (TextView) findViewById(R.id.txtCountryLongName);
        TextView calling = (TextView) findViewById(R.id.txtCallingCode);
        ImageView iv = (ImageView) findViewById(R.id.imageView);

        Button makeorkill = (Button) findViewById(R.id.btnMakeDelete);
        Button edit = (Button) findViewById(R.id.btnEdit);

        //VER O TIPO? create OU edit
        if(tipo.equals("create"))
        {
            if(Country.PAISES == null) return;

            //OBTEM O PAIS E PRENCHE OS VIEWS DE ACORDO

            pais = Country.PAISES.get(id);
            shortname.setText(pais.getShortname());
            longname.setText(pais.getLongname());
            calling.setText("("+pais.getCallingCode()+")");

            //DESABILITA O BUTAO DE EDITAR E CRIAR BUTAO DE CRIAR
            edit.setVisibility(View.GONE);
            makeorkill.setBackgroundColor(Color.BLUE);
            makeorkill.setTextColor(Color.WHITE);
            makeorkill.setText("Visitei!");

            //MOSTRAR IMAGEM
            displayImage(pais.getId());

        }
        else if(tipo.equals("edit"))
        {
            //DESABILITA BUTAO DE CRIAR E CRIA BUTAO DE EDITAR E DELETAR
            makeorkill.setBackgroundColor(Color.RED);
            makeorkill.setTextColor(Color.WHITE);
            makeorkill.setText("Remover da Lista");
            edit.setBackgroundColor(Color.GREEN);
            edit.setTextColor(Color.WHITE);
            edit.setText("Atualizar");

            //OBTER VISITADO DO BANCO DE DADOS E FIXAR ELE NA TELA
            fetchVisitado(id);
        }
    }

    public void fetchVisitado(final int id)
    {
        //OBTER TODOS OS ELEMENTOS DO VIEW
        final TextView shortname = (TextView) findViewById(R.id.txtCountryName);
        final TextView longname = (TextView) findViewById(R.id.txtCountryLongName);
        final TextView calling = (TextView) findViewById(R.id.txtCallingCode);
        final DatePicker picker = (DatePicker) findViewById(R.id.datePicker);

        final AppCompatActivity a = this;


        //CRIAR NOVA THREAD PARA OBTER DO BANC
        new Thread(new Runnable() {
            @Override
            public void run() {

                //EXECUTAR QUERY PARA OBTER O PAIS VISITADO
                VisitadoSQLHandler handler = new VisitadoSQLHandler(a);
                SQLiteDatabase db = handler.getWritableDatabase();
                visitado = VisitadoSQLHandler.getVisitadoById(db, id);
                db.close();

                //INSERIR O VISITADO OBTIDO PELO BANCO
                shortname.setText(visitado.getShortname());
                longname.setText(visitado.getLongname());
                calling.setText("("+visitado.getCallingCode()+")");

                //CONFIGURAR O DATEPICKER PARA FICAR DE ACORDO COM A DATA OBTIDA DO BANCO
                Date d = visitado.getDataVisitado();
                Calendar c = Calendar.getInstance();
                c.setTime(d);
                picker.updateDate(
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH),
                        c.get(Calendar.DAY_OF_MONTH));

                //MOSTRAR IMAGEM
                displayImage(visitado.getCountryId());
            }
        }).run();
    }

    public void SalvarPais()
    {
        //OBTER INFORMACOES NECESSARIAS PARA SALVAR O PAIS
        final DatePicker picker = (DatePicker) findViewById(R.id.datePicker);
        final Country pais = this.pais;
        final AppCompatActivity a = this;

        new Thread(new Runnable() {
            @Override
            public void run() {

                VisitadoSQLHandler handler = new VisitadoSQLHandler(a);
                SQLiteDatabase db = handler.getWritableDatabase();

                //CRIAR OBJETO VISITADO E PRENCHER ELE DE INFORMACOES
                Visitado v = new Visitado(pais);
                Calendar c = Calendar.getInstance();
                c.set(picker.getYear(), picker.getMonth(), picker.getDayOfMonth());
                v.setDataVisitado(c.getTime());

                //SALVAR NO BANCO
                VisitadoSQLHandler.insertVisitado(db, v);

                //FECHAR BANCO E CONCLUIR ATIVIDADE
                db.close();
                finish();
            }
        }).start();
    }

    public void DeletarVisitado(){
        final AppCompatActivity a = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                VisitadoSQLHandler handler = new VisitadoSQLHandler(a);
                SQLiteDatabase db = handler.getWritableDatabase();

                //DELETAR VISITADO CONFORME O OBJETO INFORMADO
                VisitadoSQLHandler.deletarVisitado(db, visitado);
                db.close();
                finish();
            }
        }).start();
    }

    protected void SaveOrKill(View v)
    {
        //DECIDE O QUE O BUTAO SAVE/KILL VAI FAZER. SE VISITADO, DELETAR, SE PAIS, SALVAR.
        final Country pais = this.pais;
        final Visitado visitado = this.visitado;

        new Thread(new Runnable() {
            @Override
            public void run() {

                if(pais != null)
                    SalvarPais();
                else if(visitado != null)
                    DeletarVisitado();
            }
        }).start();

    }
    protected void Edit(View v)
    {
        //EDITAR VISITADO.

        //RECBER E FIXAR DATA
        DatePicker d = (DatePicker) findViewById(R.id.datePicker);
        Calendar c = Calendar.getInstance();
        c.set(d.getYear(),d.getMonth(), d.getDayOfMonth());
        visitado.setDataVisitado(c.getTime());

        final AppCompatActivity a = this;

        new Thread(new Runnable() {
            @Override
            public void run() {
                VisitadoSQLHandler handler = new VisitadoSQLHandler(a);
                SQLiteDatabase db = handler.getWritableDatabase();

                //ATUALIZAR VISITADO NO BANCO
                VisitadoSQLHandler.atualizarVisitado(db, visitado);

                db.close();
                finish();
            }
        }).start();
    }

    protected void displayImage(int countryId)
    {
        //MOSTRAR BANDEIRA DO PAIS NO VIEW

        ImageView iv = (ImageView) findViewById(R.id.imageView);

        Picasso.with(this)
                .load("http://sslapidev.mypush.com.br/world/countries/"+countryId+"/flag")
                .resize((int) (80*factor),(int) (50*factor))
                .into(iv);
    }
}
