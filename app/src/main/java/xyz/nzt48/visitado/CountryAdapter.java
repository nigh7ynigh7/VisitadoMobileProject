package xyz.nzt48.visitado;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import xyz.nzt48.dados.Country;
import xyz.nzt48.dados.Visitado;

/*
*
* ADAPTADOR DE PAISES PARA O LISTVIEW
*
* */


public class CountryAdapter extends ArrayAdapter <Country>{
    public CountryAdapter(Context context, int resource) {
        super(context, resource);
    }

    public CountryAdapter(Context context, int resource, ArrayList<Country> co) {
        super(context, resource, co);
    }


    /*
     *
     * PEGAR O LAYOUT list_view_row E PRENCHER ELE (PARA DEPOIS COLOCAR NO LISTVIEW)
     *
     * */

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//        return super.getView(position, convertView, parent);

        View v = convertView;

        if(v == null)
        {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_view_row, null);
        }

        Country p = getItem(position);

        if(p!= null)
        {
            TextView view = (TextView) v.findViewById(R.id.txt);
            view.setTextSize(20);
            ImageView image = (ImageView) v.findViewById(R.id.img);

            if(view!=null)
                view.setText(p.toString());
            if(image != null)
            {
                double mult = 1;
                int x, y;
                x = (int) (80*mult);
                y = (int) (50*mult);

                Picasso.with(getContext())
                        .load("http://sslapidev.mypush.com.br/world/countries/"+p.getId()+"/flag")
                        .resize(x,y)
                        .into(image);
            }
        }

        return v;
    }

}
