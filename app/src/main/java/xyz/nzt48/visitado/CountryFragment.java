package xyz.nzt48.visitado;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;

import us.monoid.web.Resty;
import xyz.nzt48.dados.Country;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CountryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CountryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CountryFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CountryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CountryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CountryFragment newInstance(String param1, String param2) {
        CountryFragment fragment = new CountryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        //OBTER PAISES VINDO DE FORA!
        new GetCountryList().execute("http://sslapidev.mypush.com.br/world/countries/active/");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_country, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    /*
    *
    *   ASYNCTASK PARA OBTER A LISTA DE PAISES E COLOCAR O LISTVIEWER
    *
    * */

    class GetCountryList extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            if(params.length != 1)
                return null;

            try {
                //TENTA BAIXAR A LISTA E RETORNA O JSON
                return  new Resty().text(params[0]).toString();
            }
            catch (IOException e)
            {
                return "Error while fetching code";
            }
        }

        @Override
        protected void onPostExecute(String result){


            //CONVERTER JSON -> LISTA DE PAISES E COLOCAR NO MEMBRO ESTATICO Country.PAISES
            Country.CriarPaises(result);

            if(Country.PAISES != null)
            {
                //SE HOUVER PAISES, COLOCAR NO ListView
                if(Country.PAISES.size() > 0) {
                    setList(Country.PAISES);
                }
            }

        }

        public void setList(ArrayList<Country> paises)
        {
            ListView lv = (ListView) getView().findViewById(R.id.listaPaises);
            if(lv == null) return;

//            ArrayAdapter<Country> adapter = new ArrayAdapter<>(
//                    lv.getContext(),
//                    android.R.layout.simple_list_item_1,
//                    paises);


            //CRIAR ADAPTADOR PARA A LISTA DE PAISES E COLOCAR NO ListView lv
            CountryAdapter adapter = new CountryAdapter(getContext(), R.layout.list_view_row,paises);
            lv.setAdapter(adapter);

            //CRIAR OBSERVADOR DE TOQUES VINDO DO USUÁRIO E COLOCAR NO ListView lv
            AdapterView.OnItemClickListener ouvidor = new AdapterView.OnItemClickListener(){
                public void onItemClick(AdapterView<?> listView,
                                        View itemView,
                                        int position,
                                        long id) {

                    //SE HOUVER TOQUES, INICIAR UMA NOVA ATIVIDADE PARA INSERIR OS PAISES NO BANCO
                    Intent intent = new Intent(getActivity(), VisitadoActivity.class);
                    intent.putExtra("tipo", "create");
                    intent.putExtra("id", (int) id);
                    startActivity(intent);
                }
            };

            lv.setOnItemClickListener(ouvidor);
        }
    }
}
