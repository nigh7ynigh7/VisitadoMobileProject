package xyz.nzt48.visitado;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/*
*   MAKING MULTIUSED TAB PAGES:
*
* https://www.learn2crack.com/2013/10/android-custom-listview-images-text-example.html
* https://www.caveofprogramming.com/guest-posts/custom-listview-with-imageview-and-textview-in-android.html
*
*
* */




public class MainTabbedActivity extends AppCompatActivity {


    private TabPageAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tabbed);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new TabPageAdapter(getSupportFragmentManager(), this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);


        //Colocar os fragmentos de Perfil, Paises, e Visitados no adaptador de abas.
        if(mSectionsPagerAdapter.getCount() == 0) {
            mSectionsPagerAdapter.addItem(new PerfilFragment());
            mSectionsPagerAdapter.addItem(new CountryFragment());
            mSectionsPagerAdapter.addItem(new VisitadosFragment());
        }


        //colocar as abas no container dos mesmos
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_tabbed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
