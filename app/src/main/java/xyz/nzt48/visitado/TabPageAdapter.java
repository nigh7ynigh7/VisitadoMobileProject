package xyz.nzt48.visitado;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
//import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/*
*
* ADAPTADOR DE FRAGMENTOS DE ABAS.
* PARA GERENCIAR AS ABAS PRINCIPAIS
*
* */

public class TabPageAdapter extends FragmentPagerAdapter
{

    public static ArrayList<Fragment> fragments = null;
    public AppCompatActivity activity;

    public TabPageAdapter(FragmentManager fm, AppCompatActivity activity) {
        super(fm);
        this.activity = activity;
    }


    @Override
    public Fragment getItem(int position) {
        if(fragments == null) return null;

        return fragments.get(position);
    }

    @Override
    public int getCount() {
        if(fragments == null)
            return 0;
        return fragments.size();
    }

    public void addItem(Fragment frag)
    {
        if(fragments == null)
            fragments = new ArrayList<>();
        fragments.add(frag);
    }


}
